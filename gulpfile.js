var gulp = require('gulp')
var notify = require('gulp-notify')
var rollup = require('rollup-stream')
var mocha = require('gulp-spawn-mocha')
var buble = require('rollup-plugin-buble')
var source = require('vinyl-source-stream')
var replace = require('rollup-plugin-replace')
var commonjs = require('rollup-plugin-commonjs')
var nodeResolve = require('rollup-plugin-node-resolve')

gulp.task('default', () =>
  rollup({
    entry: './src/main.js',
    format: 'iife',
    moduleName: 'javiDevBundle',
    plugins: [
      nodeResolve({ browser: true }),
      commonjs({
        include: [
          'node_modules/**',
          'src/**'
        ]
      }),
      replace({
        'process.env.NODE_ENV': JSON.stringify('development') // or 'production'
      }),
      buble()
    ]
  })
    .pipe(source('app.js'))
    .pipe(gulp.dest('./public/js'))
)

gulp.task('test', () =>
  gulp.src('test/**/*.js', {read: false})
    .pipe(mocha({
      require: 'reify'
    }))
    .on('error', notify.onError('RED!'))
)

gulp.task('tdd', () =>
  gulp.watch('{src,test}/**/*.js', ['test'])
)

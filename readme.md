# JavaScript Testing Setup

Simple folder structure, `package.json` and `gulpfile.js` to quickly create a JavaScript project with TDD in mind, using mocha, chai, rollup…

## Objectives

* Quickly bundle everything and transpile for the browser.
* Production code goes in the `public` folder. JavaScript is bundled to `public\js\app.js`.
* Prepare for the future ES6 module imports, using reify.
* Allow individual test files in the testing folder, using the modules' source code in the `src` folder.

## Usage

1. Clone the repo.
2. Change `package.json` details. `npm install`
2. Write code in the `src` folder, using whatever desired folder structure ('components' folder provided as example).
3. Use a `main.js` file as an entry point, importing all the modules and setting up the world.
4. The idea is to replicate the same structure in the testing folder, and to import the modules in the `src` folder as needed (again, 'components' folder provided as an example).
5. Use `gulp`, `gulp test`, and `gulp tdd`, to build for production, test, and watch, respectively.